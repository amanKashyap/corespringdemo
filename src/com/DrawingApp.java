package com;


import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class DrawingApp {

	public static void main(String[] args) 
	{
		//Without Spring
		/*Triangle tri = new Triangle();
		tri.draw();*/
		
		//With Spring
		/*BeanFactory factory = new XmlBeanFactory(new FileSystemResource("src\\spring.xml"));*/
		
		AbstractApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
		context.registerShutdownHook();
		/*Factory using the Spring.xml as a blueprint to instantiate the object of Triangle class which can
		be fetched using the getBean() by passing the id specified in spring.xml.*/
		Shape shape = (Shape) context.getBean("circle");
		shape.draw();
		
		//As the prototype is Singleton. Same object is getting returned hence the setter method not called again.
		/*Triangle triA = (Triangle) context.getBean("triangle-alias");
		Triangle triN = (Triangle) context.getBean("triangle-name");*/
		//Can't create object of Point because defined under property of Triangle.
		/*Point point =(Point) factory.getBean("zeroPoint");*/
		
	  /*TriangleSet triangleSet = (TriangleSet) context.getBean("triangleSet");
		triangleSet.draw();*/
	}

}
