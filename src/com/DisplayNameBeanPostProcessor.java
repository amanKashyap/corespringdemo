package com;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

/*A Bean post processor is called for every bean created from spring.xml*/
public class DisplayNameBeanPostProcessor implements BeanPostProcessor 
{

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException 
	{
		System.out.println("BeanPostProcessor postProcessAfterInitialization BeanName: "+beanName);
		return bean;
	}

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException 
	{
		System.out.println("BeanPostProcessor postProcessBeforeInitialization BeanName: "+beanName);
		return bean;
	}

}
