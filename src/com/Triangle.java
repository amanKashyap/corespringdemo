package com;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class Triangle implements Shape,ApplicationContextAware,BeanNameAware,InitializingBean,DisposableBean
{
	private Point pointA;
	private Point pointB;
	private Point pointC;
	private ApplicationContext context;
	
	public void draw()
	{	
		System.out.println("Point A= ("+getPointA().getX()+","+getPointA().getY()+")");
		System.out.println("Point B= ("+getPointB().getX()+","+getPointB().getY()+")");
		System.out.println("Point C= ("+getPointC().getX()+","+getPointC().getY()+")");
		
		/*System.out.println("Triangle Drawn :"+getType());
		System.out.println("Having height :"+getHeight())*/;
	}

	public Point getPointA() {
		return pointA;
	}

	public void setPointA(Point pointA) {
		System.out.println("Setter of Point A");
		this.pointA = pointA;
	}

	public Point getPointB() {
		return pointB;
	}

	public void setPointB(Point pointB) {
		System.out.println("Setter of Point B");
		this.pointB = pointB;
	}

	public Point getPointC() {
		return pointC;
	}

	public void setPointC(Point pointC) {
		System.out.println("Setter of Point C");
		this.pointC = pointC;
	}
	/*private String type;
	private int height;
	
	//Constructor Injection.
	public Triangle(String type)
	{
		System.out.println("One-arg Constructor having parameter type called");
		this.type=type;
	}
	
	public Triangle(int height, String type)
	{
		System.out.println("Construtor 1");
		this.type=type;
		this.height=height;
	}
	
	public Triangle(String type,int height)
	{
		System.out.println("Construtor 2");
		this.type=type;
		this.height=height;
	}
	
	public int getHeight() {
		return height;
	}

	public String getType() {
		return type;
	}
	//Used for Setter Injection.
	public void setType(String type) {
		System.out.println("type setter method called");
		this.type = type;
	}*/


	@Override
	public void setApplicationContext(ApplicationContext arg0) throws BeansException {
		System.out.println("ApplicationContextAware");
		this.context=arg0;				
	}
	@Override
	public void setBeanName(String arg0) 
	{
		System.out.println("BeanNameAware");
	}

	@Override
	public void destroy() throws Exception {
		System.out.println("Disposable Bean");		
	}
	
	public void cleanup()
	{
		System.out.println("Destroy method called from Spring.xml");
	}
	
	public void defaultCleanup()
	{
		System.out.println("Default Destroy method from Spring.xml");
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		System.out.println("Initializing Bean");
		
	}
	
	public void myInit()
	{
		System.out.println("Init method called from spring.xml");
	}
	
	public void defaultMyInit()
	{
		System.out.println("Default init method called from spring.xml");
	}

}
